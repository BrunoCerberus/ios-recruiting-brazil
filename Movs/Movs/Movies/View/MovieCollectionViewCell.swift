//
//  MovieCollectionViewCell.swift
//  Movs
//
//  Created by Bruno Lopes de Mello on 29/04/19.
//  Copyright © 2019 Bruno Lopes de Mello. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var loaderIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    @IBAction func favoriteOrUnfavoriteMovie(_ sender: Any) {
        if favoriteButton.isSelected {
            favoriteButton.isSelected = false
        } else {
            favoriteButton.isSelected = true
        }
    }
    
    func fillCell(movie: Movie) {
        movieImage.sd_setImage(with: URL(string: URLs.baseImagesUrl +
            (movie.posterPath ?? ""))) { [unowned self] (image, error, cache, url) in
                DispatchQueue.main.async {
                    self.loaderIndicator.stopAnimating()
                }
        }
        
    }
}
