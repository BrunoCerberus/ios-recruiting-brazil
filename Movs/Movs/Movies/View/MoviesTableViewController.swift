//
//  MoviesTableViewController.swift
//  Movs
//
//  Created by Bruno Lopes de Mello on 28/04/19.
//  Copyright © 2019 Bruno Lopes de Mello. All rights reserved.
//

import UIKit
import iCarousel
import SDWebImage

class MoviesTableViewController: UITableViewController, Storyboarded {

    @IBOutlet weak var refresher: UIRefreshControl!
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var topRatedCollection: UICollectionView!
    @IBOutlet weak var popularCollection: UICollectionView!
    
    var nowPlayingMovies = [Movie]() {
        didSet {
            
            pageControl.numberOfPages = self.nowPlayingMovies.count
            carouselView.reloadData()
        }
    }
    
    var topRatedMovies = [Movie]() {
        didSet {
            topRatedCollection.reloadData()
        }
    }
    
    var popularMovies = [Movie]() {
        didSet {
            popularCollection.reloadData()
        }
    }
    
    var viewModel: MovieViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = MovieViewModel()

        loadData((Any).self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    @IBAction func loadData(_ sender: Any) {
        //Now Playing movies
        viewModel?.getNowPlayingMovies(onSuccess: { [unowned self] movies in
            DispatchQueue.main.async {
                self.refresher.endRefreshing()
                self.nowPlayingMovies = movies
            }
        }, onFail: { (msg) in
            self.refresher.endRefreshing()
            GlobalAlert(with: self, msg: msg).show()
        })
        
        //Top Rated movies
        viewModel?.getTopRatedMovies(onSuccess: { [unowned self] movies in
            DispatchQueue.main.async {
                self.topRatedMovies = movies
            }
            }, onFail: { (msg) in
                GlobalAlert(with: self, msg: msg).show()
        })
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
}


// MARK: - <#iCarouselDataSource, iCarouselDelegate#>
extension MoviesTableViewController: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return nowPlayingMovies.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        //create the UIView
        let tempView = UIView(frame: CGRect(x: 0, y: 0, width: carouselView.bounds.width, height: 200))
        
        //crate a UIImageView
        let frame = CGRect(x: 0, y: 0, width: carouselView.bounds.width, height: 200)
        let imageView = UIImageView()
        imageView.frame = frame
        imageView.contentMode = .scaleAspectFill
        
        //set the images to the imageview and add it to the tempView
        imageView.sd_setImage(with: URL(string: URLs.baseImagesUrl + (nowPlayingMovies[index].backdropPath ?? "")), completed: nil)
        tempView.addSubview(imageView)
        
        return tempView
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.pageControl.currentPage = carousel.currentItemIndex
    }
}


// MARK: - <#UICollectionViewDataSource, UICollectionViewDelegate#>
extension MoviesTableViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return collectionView == topRatedCollection ? topRatedMovies.count : 20
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == topRatedCollection {
            //Top Rated collection
            guard let cell = topRatedCollection
                .dequeueReusableCell(withReuseIdentifier: "topCell", for: indexPath)
                as? MovieCollectionViewCell else {return UICollectionViewCell()}
            let movie = topRatedMovies[indexPath.row]
            cell.fillCell(movie: movie)
            return cell
        } else {
            //Popular Collection
            guard let cell = popularCollection
                .dequeueReusableCell(withReuseIdentifier: "popularCell", for: indexPath)
                as? MovieCollectionViewCell else {return UICollectionViewCell()}
            
            return cell
        }
    }
}
