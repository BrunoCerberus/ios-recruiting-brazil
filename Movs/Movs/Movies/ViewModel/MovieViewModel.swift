//
//  MovieViewModel.swift
//  Movs
//
//  Created by Bruno Lopes de Mello on 28/04/19.
//  Copyright © 2019 Bruno Lopes de Mello. All rights reserved.
//

import Foundation

protocol MovieViewModelProtocol: class {
    func getNowPlayingMovies(onSuccess: @escaping ([Movie]) -> Void,
                             onFail: @escaping (_ msg: String) -> Void)
    func getTopRatedMovies(onSuccess: @escaping ([Movie]) -> Void,
                             onFail: @escaping (_ msg: String) -> Void)
}

class MovieViewModel: MovieViewModelProtocol {
    
    
    /// Retrieve the first page of Now Playing movies to set into carousel
    ///
    /// - Parameters:
    ///   - onSuccess: onSuccess description
    ///   - onFail: onFail description
    func getNowPlayingMovies(onSuccess: @escaping ([Movie]) -> Void, onFail: @escaping (String) -> Void) {
        let paremeters: [String: Any] = [:]
        
        Api().requestCodable(metodo: .wGET, url: URLs.getNowPlaying,
                             objeto: MoviesReturn.self, parametros: paremeters,
                             onSuccess: { (response, result) in
                                
                                if result.results?.count == 0 || result.results == nil {
                                    onFail("Not possible to load any movies")
                                    return
                                }
                                
                                onSuccess(result.results ?? [Movie]())
                                
        }) { (response, error) in
            onFail(error)
        }
    }
    
    
    /// Retrieve the first page of Top Rated Movies
    ///
    /// - Parameters:
    ///   - onSuccess: onSuccess description
    ///   - onFail: onFail description
    func getTopRatedMovies(onSuccess: @escaping ([Movie]) -> Void, onFail: @escaping (String) -> Void) {
        let paremeters: [String: Any] = [:]
        
        Api().requestCodable(metodo: .wGET, url: URLs.getTopRated,
                             objeto: MoviesReturn.self, parametros: paremeters,
                             onSuccess: { (response, result) in
                                
                                if result.results?.count == 0 || result.results == nil {
                                    onFail("Not possible to load any movies")
                                    return
                                }
                                
                                onSuccess(result.results ?? [Movie]())
                                
        }) { (response, error) in
            onFail(error)
        }
    }
    
    /// Retrieve popular movies with paging options
    ///
    /// - Parameters:
    ///   - onSuccess: onSuccess description
    ///   - onFail: onFail description
    func getPopularMovies(onSuccess: @escaping ([Movie]) -> Void, onFail: @escaping (String) -> Void) {
        let paremeters: [String: Any] = [:]
        
        Api().requestCodable(metodo: .wGET, url: URLs.getPopular,
                             objeto: MoviesReturn.self, parametros: paremeters,
                             onSuccess: { (response, result) in
                                
                                if result.results?.count == 0 || result.results == nil {
                                    onFail("Not possible to load any movies")
                                    return
                                }
                                
                                onSuccess(result.results ?? [Movie]())
                                
        }) { (response, error) in
            onFail(error)
        }
    }
}
